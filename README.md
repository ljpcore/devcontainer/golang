# golang

Provides a docker image that can be used for Go development using Visual Studio
Code Development Containers.

## Usage

```json
{
    "image": "registry.gitlab.com/ljpcore/devcontainer/golang:1.17.5"
}
```
