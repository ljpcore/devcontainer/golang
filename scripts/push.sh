#!/bin/bash
set -e

GOLANG_VERSION="1.17.5"

echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
docker build --build-arg GOLANG_VERSION=$GOLANG_VERSION -t $CI_REGISTRY/ljpcore/devcontainer/golang:$GOLANG_VERSION .
docker push $CI_REGISTRY/ljpcore/devcontainer/golang:$GOLANG_VERSION